package com.example.rattan_bhavya.assignment5_jsonpractice.activities;

/**
 * Created by RATTAN-BHAVYA on 2/2/2015.
 */
public class FriendActivity {
    public String name;
    public String friendId;

    public FriendActivity(String friendId, String name) {
        this.friendId = friendId;
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;

    }
}
