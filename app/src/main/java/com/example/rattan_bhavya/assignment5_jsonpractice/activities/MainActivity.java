package com.example.rattan_bhavya.assignment5_jsonpractice.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.rattan_bhavya.assignment5_jsonpractice.utils.FacebookUtils;
import com.example.rattan_bhavya.assignment5_jsonpractice.R;
import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends FragmentActivity {

    List<FriendActivity> friendActivityList;
    ListView friendListView;
    ArrayAdapter<FriendActivity> listAdapter;
    Intent friendDetailsItem;
    String friendId;
    String firstName;
    String lastName;
    String gender;
    String link;
    String accessToken;
    // String url = "https://graph.facebook.com/me/friends?access_token=" + access_token;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        friendActivityList = new ArrayList<FriendActivity>();
        friendListView = (ListView) findViewById(R.id.friendListView);
        listAdapter = new ArrayAdapter<FriendActivity>(this, R.layout.friend_list_item, friendActivityList);
        friendListView.setAdapter(listAdapter);

        FacebookUtils.fbLogin(0, MainActivity.this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode,
                resultCode, data);
    }

    public void getFriends(String fbAccessToken) {
        accessToken = fbAccessToken;
        String url = "https://graph.facebook.com/me/friends?access_token=" + fbAccessToken;
        final AsyncHttpClient friendClient = new AsyncHttpClient();
        friendClient.get(MainActivity.this, url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                friendActivityList.clear();
                try {
                    JSONObject friendNameList = new JSONObject(s);
                    JSONArray data = friendNameList.optJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject friend = data.getJSONObject(i);
                        String name = friend.getString("name");
                        friendId = friend.getString("id");
                        friendActivityList.add(new FriendActivity(friendId, name));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                listAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                Toast.makeText(MainActivity.this, "Failed to fetch friend name", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnExit:
                onBackPressed();
                break;
            default:
                break;
        }
        friendListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                friendId = friendActivityList.get(position).friendId;
                String uri = "https://graph.facebook.com/" + friendId + "?access_token=" + accessToken;
                final AsyncHttpClient friendDetailsClient = new AsyncHttpClient();

                friendDetailsClient.get(MainActivity.this, uri, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(String details) {
                        super.onSuccess(details);

                        try {

                            JSONObject friendDetails = new JSONObject(details);
                            firstName = friendDetails.getString("first_name");
                            lastName = friendDetails.getString("last_name");
                            gender = friendDetails.getString("gender");
                            link = friendDetails.getString("link");
                            friendDetailsItem = new Intent(MainActivity.this, FriendDetailsActivity.class);
                            friendDetailsItem.putExtra("firstName", firstName);
                            friendDetailsItem.putExtra("lastName", lastName);
                            friendDetailsItem.putExtra("gender", gender);
                            friendDetailsItem.putExtra("link", link);
                            startActivity(friendDetailsItem);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String s) {
                        super.onFailure(throwable, s);

                        Toast.makeText(MainActivity.this, "Failed to fetch friend details", Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Do you really want to Exit?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user pressed "yes", then he is allowed to exit from application
                finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }


}
