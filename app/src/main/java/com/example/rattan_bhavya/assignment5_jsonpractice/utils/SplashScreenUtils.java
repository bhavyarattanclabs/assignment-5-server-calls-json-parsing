package com.example.rattan_bhavya.assignment5_jsonpractice.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.example.rattan_bhavya.assignment5_jsonpractice.R;
import com.example.rattan_bhavya.assignment5_jsonpractice.activities.MainActivity;


public class SplashScreenUtils extends Activity {

    /**
     * Duration of wait *
     */
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    /**
     * Called when the activity is first created.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                startActivity(new Intent(SplashScreenUtils.this, MainActivity.class));
                finish();
            }
        }, secondsDelayed * 3000);
    }
}
