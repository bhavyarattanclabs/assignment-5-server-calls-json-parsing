package com.example.rattan_bhavya.assignment5_jsonpractice.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.rattan_bhavya.assignment5_jsonpractice.R;


public class FriendDetailsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_details);

        String firstName = getIntent().getStringExtra("firstName");
        String lastName = getIntent().getStringExtra("lastName");
        String gender = getIntent().getStringExtra("gender");
        String link = getIntent().getStringExtra("link");

        TextView oTvFirstName = (TextView) findViewById(R.id.tvFirstName);
        TextView oTvLastName = (TextView) findViewById(R.id.tvLastName);
        TextView oTvGender = (TextView) findViewById(R.id.tvGender);
        TextView oTvLink = (TextView) findViewById(R.id.tvTimelineLink);

        oTvFirstName.setText(firstName);
        oTvLastName.setText(lastName);
        oTvGender.setText(gender);
        oTvLink.setText(link);
    }

    public void back(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                finish();
                break;
            default:
                break;
        }
    }

}
